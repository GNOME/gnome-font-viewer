# translation of si.po to Sinhala
# Danishka Navin <snavin@redhat.com>, 2007.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Translators Danishka Navin <danishka@gmail.com>, Harshana Weerasinghe  <me@harshana.info>
msgid ""
msgstr ""
"Project-Id-Version: si\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-10-06 14:36-0400\n"
"PO-Revision-Date: 2007-09-17 09:07+0530\n"
"Last-Translator: Danishka Navin <snavin@redhat.com>\n"
"Language-Team: Sinhala <en@li.org>\n"
"Language: si\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: KBabel 1.11.4\n"

#: ../src/font-thumbnailer.c:267
msgid "Text to thumbnail (default: Aa)"
msgstr ""

#: ../src/font-thumbnailer.c:267
msgid "TEXT"
msgstr ""

#: ../src/font-thumbnailer.c:269
msgid "Font size (default: 64)"
msgstr ""

#: ../src/font-thumbnailer.c:269
msgid "SIZE"
msgstr ""

#: ../src/font-thumbnailer.c:271
msgid "FONT-FILE OUTPUT-FILE"
msgstr ""

#: ../src/font-view.c:289
#, fuzzy
msgid "Name:"
msgstr "නම (_N):"

#: ../src/font-view.c:292
msgid "Style:"
msgstr ""

#: ../src/font-view.c:304
#, fuzzy
msgid "Type:"
msgstr "වර්‍ගය"

#: ../src/font-view.c:308
#, fuzzy
msgid "Size:"
msgstr "ප්‍රමාණය"

#: ../src/font-view.c:352 ../src/font-view.c:365
#, fuzzy
msgid "Version:"
msgstr "වෙළුම: "

#: ../src/font-view.c:356 ../src/font-view.c:367
msgid "Copyright:"
msgstr ""

#: ../src/font-view.c:360
#, fuzzy
msgid "Description:"
msgstr "විස්තරය (_D):"

#: ../src/font-view.c:428
msgid "Installed"
msgstr ""

#: ../src/font-view.c:430
msgid "Install Failed"
msgstr ""

#: ../src/font-view.c:502
#, c-format
msgid "Usage: %s fontfile\n"
msgstr ""

#: ../src/font-view.c:572
msgid "I_nstall Font"
msgstr ""

#: ../src/gnome-font-viewer.desktop.in.in.h:1
#, fuzzy
msgid "Font Viewer"
msgstr "ලැයිස්තු දසුන"

#: ../src/gnome-font-viewer.desktop.in.in.h:2
#, fuzzy
msgid "Preview fonts"
msgstr "ඉදිරිපිටුව මුද්‍රණය කරන්න (_P):"
